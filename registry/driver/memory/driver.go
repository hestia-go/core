// Package memory provides memory implementation of the `RegistryDriver`
//
// You can register an instance of the memory driver by using the `RegisterDriver` of the `registry` package:
//
//	Register("custom_memory", NewMemory)
package memory

import (
	"sync"
	"time"

	"gitlab.com/hestia-go/core/constant"
	_interface "gitlab.com/hestia-go/core/registry/interface"
)

// Driver structure
type Driver struct {
	data         map[string]any
	timers       map[string]*time.Timer
	mutexRW      sync.RWMutex
	mutexTimerRW sync.RWMutex
}

// Get Return the value of the given key name
func (m *Driver) Get(name string) any {
	m.mutexRW.RLock()
	defer m.mutexRW.RUnlock()
	value, ok := m.data[name]

	if !ok {
		return nil
	}

	return value
}

// Has Return if the key name is available in this driver instance
func (m *Driver) Has(name string) bool {
	m.mutexRW.RLock()
	defer m.mutexRW.RUnlock()
	_, ok := m.data[name]

	return ok
}

// Remove the key value pair from the registry
func (m *Driver) Remove(name string) bool {
	defer m.removeTimer(name)
	m.mutexRW.Lock()
	defer m.mutexRW.Unlock()
	delete(m.data, name)

	return true
}

// SetWithTTL Set a key value pair in registry and apply a TTL on it, if you pass -1 no TTL is applied
func (m *Driver) SetWithTTL(name string, value any, ttl time.Duration) bool {
	m.mutexRW.RLock()
	_, ok := m.data[name]
	m.mutexRW.RUnlock()

	if ok {
		m.Remove(name)
	}

	m.mutexRW.Lock()
	m.data[name] = value
	m.mutexRW.Unlock()

	defer m.addTimer(name, ttl)

	return true
}

// Set Shorthand function to set a key value pair with TTL to -1, see SetWithTTL for more information
func (m *Driver) Set(name string, value any) bool {
	return m.SetWithTTL(name, value, -1)
}

// addTimer Add a timer for the given key name, at the end of the timer the key is destroyed
func (m *Driver) addTimer(name string, ttl time.Duration) {
	if ttl == -1 {
		return
	}

	m.removeTimer(name)

	timer := time.AfterFunc(ttl, func () {
		m.Remove(name)
	})

	m.mutexTimerRW.RLock()
	m.timers[name] = timer
	m.mutexTimerRW.RUnlock()
}

// removeTimer Remove a timer associated to the given key name
func (m *Driver) removeTimer(name string) {
	m.mutexTimerRW.RLock()
	timer, ok :=  m.timers[name]
	m.mutexTimerRW.RUnlock()

	if !ok {
		return
	}

	timer.Stop()

	m.mutexTimerRW.Lock()
	defer m.mutexTimerRW.Unlock()
	delete(m.timers, name)
}

// NewMemory Create an instance of memory driver
func NewMemory(_ any) _interface.RegistryDriver {
	return &Driver{
		data:   map[string]any{},
		timers: map[string]*time.Timer{},
	}
}

// RegistryToken Return the registration token of this driver for the registry system
func RegistryToken() string {
	return constant.RegistryMemoryDriverToken
}
