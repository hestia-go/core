package memory

import (
	"testing"
	"time"

	"gitlab.com/hestia-go/core/constant"
)

func TestDriver_SetWithTTL(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	value := "value"
	ttl := time.Duration(1 * time.Second)

	d.SetWithTTL(name, value, ttl)

	if !d.Has(name) {
		t.Errorf("expected to have %s", name)
	}

	if d.Get(name) != value {
		t.Errorf("expected %s but got %s", value, d.Get(name))
	}

	time.Sleep(2 * time.Second)

	if d.Has(name) {
		t.Errorf("expected not to have %s", name)
	}
}

func TestDriver_SetWithTTL_AlreadyExistingKey(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	value := "value"
	value2 := "value2"
	ttl := time.Duration(1 * time.Second)

	d.Set(name, value)

	if !d.Has(name) {
		t.Errorf("expected to have %s", name)
	}

	if d.Get(name) != value {
		t.Errorf("expected %s but got %s", value, d.Get(name))
	}

	d.SetWithTTL(name, value2, ttl)

	if !d.Has(name) {
		t.Errorf("expected to have %s", name)
	}

	if d.Get(name) != value2 {
		t.Errorf("expected %s but got %s", value2, d.Get(name))
	}

	time.Sleep(2 * time.Second)

	if d.Has(name) {
		t.Errorf("expected not to have %s", name)
	}
}

func TestDriver_Set(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	value := "value"

	d.Set(name, value)

	if !d.Has(name) {
		t.Errorf("expected to have %s", name)
	}

	if d.Get(name) != value {
		t.Errorf("expected %s but got %s", value, d.Get(name))
	}
}

func TestDriver_Remove(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	value := "value"

	d.Set(name, value)
	d.Remove(name)

	if d.Has(name) {
		t.Errorf("expected not to have %s", name)
	}

	if d.Get(name) != nil {
		t.Errorf("expected nil but got %v", d.Get(name))
	}
}

func TestDriver_addTimer(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	ttl := time.Duration(1 * time.Second)

	if md, ok := d.(*Driver); ok {
		md.addTimer(name, ttl)

		if md.timers[name] == nil {
			t.Errorf("expected to have timer %s", name)
		}
	}
}

func TestDriver_removeTimer(t *testing.T) {
	d := NewMemory(nil)
	name := "key"
	value := "value"
	ttl := time.Duration(1 * time.Second)

	d.SetWithTTL(name, value, ttl)

	if md, ok := d.(*Driver); ok {
		md.removeTimer(name)

		if md.timers[name] != nil {
			t.Errorf("expected to not have timer %s", name)
		}
	}
}

func TestNewMemory(t *testing.T) {
	driver := NewMemory(nil)
	if driver == nil {
		t.Errorf("NewMemory should not return nil")
	}
}

func TestRegistryToken(t *testing.T) {
	token := RegistryToken()
	if token != constant.RegistryMemoryDriverToken {
		t.Errorf("RegistryToken should return %s but got %s", constant.RegistryMemoryDriverToken, token)
	}
}
