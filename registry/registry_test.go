package registry

import (
	"testing"

	"gitlab.com/hestia-go/core/registry/driver/memory"
)

func TestInitRegistry(t *testing.T) {
	// Call InitRegistry to initialize the system
	InitRegistry()

	// Check that the Memory driver was registered and a Memory instance was created
	registry := GetInstance(memory.RegistryToken())
	if registry == nil {
		t.Errorf("Memory registry not registered")
	} else {
		_, ok := registry.(*memory.Driver)
		if !ok {
			t.Errorf("Registry instance is not of type Memory")
		}
	}

	// Check that the registry is not initialized again
	InitRegistry()
	registry2 := GetInstance(memory.RegistryToken())
	if registry2 == nil {
		t.Errorf("Memory registry not registered")
	} else if registry != registry2 {
		t.Errorf("Registry was initialized again")
	}
}

func TestGetInstance(t *testing.T) {
	// Register a memory driver
	Register(memory.RegistryToken(), memory.NewMemory)

	// Get an instance of the memory driver
	registry := GetInstance(memory.RegistryToken())

	// Check that the returned registry is not nil
	if registry == nil {
		t.Errorf("GetInstance returned nil for valid driver name")
	}

	// Check that getting an instance with an invalid driver name returns nil
	registry = GetInstance("invalid_driver")
	if registry != nil {
		t.Errorf("GetInstance returned non-nil for invalid driver name")
	}
}

func TestGetInstanceWithOptions(t *testing.T) {
	// Register a memory driver
	Register(memory.RegistryToken(), memory.NewMemory)

	// Get an instance of the memory driver
	registry := GetInstanceWithOptions(memory.RegistryToken(), nil)

	// Check that the returned registry is not nil
	if registry == nil {
		t.Errorf("GetInstanceWithOptions returned nil for valid driver name")
	}

	// Check that getting an instance with an invalid driver name returns nil
	registry = GetInstanceWithOptions("invalid_driver", nil)
	if registry != nil {
		t.Errorf("GetInstanceWithOptions returned non-nil for invalid driver name")
	}
}
