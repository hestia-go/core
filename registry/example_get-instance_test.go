package registry_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/registry"
	"gitlab.com/hestia-go/core/registry/driver/memory"
)

func ExampleGetInstance() {
	registry.InitRegistry()
	instance := registry.GetInstance(memory.RegistryToken())

	instance.Set("my_key", "my value")

	fmt.Println(instance.Get("my_key"))

	// Output:
	// my value
}
