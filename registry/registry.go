// Package registry provides registry mechanism
// This lets you store any volatile information during the application runtime.
//
// Builtin registry driver: Memory
//
// It's possible to create custom registry driver by implementing the `RegistryDriver` interface available in the sub-package `interface`.
//
// For a registry to be available, it needs to be registered through the `Register` function.
package registry

import (
	"sync"

	"gitlab.com/hestia-go/core/is"
	"gitlab.com/hestia-go/core/registry/driver/memory"
	_interface "gitlab.com/hestia-go/core/registry/interface"
)

// Registry RegistryManager object structure
type Registry struct {
	registries    map[string]_interface.RegistryDriver
	registryTypes map[string]func(opts any) _interface.RegistryDriver
	mutex         sync.RWMutex
}

// r Global registry manager instance
var r = &Registry{
	registries:    map[string]_interface.RegistryDriver{},
	registryTypes: map[string]func(opts any) _interface.RegistryDriver{},
	mutex:         sync.RWMutex{},
}

// initialized State of the initialization
var initialized = false

// InitRegistry Initialize the registry system with the defaults driver
func InitRegistry() {
	if !initialized {
		initialized = true
		Register(memory.RegistryToken(), memory.NewMemory)
	}
}

// Register a driver into the registry system
func Register(name string, factory func(opts any) _interface.RegistryDriver) {
	r.registryTypes[name] = factory
}

// GetInstance Return the instance of the given name or create a new with a nil option
func GetInstance(name string) _interface.RegistryDriver {
	return GetInstanceWithOptions(name, nil)
}

// GetInstanceWithOptions Return the instance of the given name or create a new one with the given option if it not exists
// Currently no internal driver use options
func GetInstanceWithOptions(name string, opts any) _interface.RegistryDriver {
	r.mutex.RLock()
	registry := r.registries[name]
	r.mutex.RUnlock()

	if is.Nil(registry) {
		r.mutex.Lock()
		defer r.mutex.Unlock()
		if is.Nil(r.registryTypes[name]) {
			return nil
		}

		registry = r.registryTypes[name](opts)
		r.registries[name] = registry
	}

	return registry
}
