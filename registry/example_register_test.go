package registry_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/registry"
	"gitlab.com/hestia-go/core/registry/driver/memory"
)

func ExampleRegister() {
	registry.Register("example_registry", memory.NewMemory)

	// Get the newly registered registry
	reg := registry.GetInstance("example_registry")

	reg.Set("example key", "example value")
	fmt.Println(reg.Get("example key"))

	// Output:
	// example value
}
