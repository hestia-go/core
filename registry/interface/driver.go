package _interface

import "time"

// RegistryDriver Interface to implement in order to be able to register a driver into the registry system
type RegistryDriver interface {
	// Get returns the value attached to the given key name
	Get(name string) any
	// Set a value for the given key name
	Set(name string, value any) bool
	// SetWithTTL Set a value for the given key name, this key will expire after the given ttl
	SetWithTTL(name string, value any, ttl time.Duration) bool
	// Remove a key and its value from the registry
	Remove(name string) bool
	// Has returns the key existence
	Has(name string) bool
}
