# Hestia Core
Package Hestia Core contains tools that becomes available in all Hestia based projects.

## What's included
* Exception factory
* Registry
* Utility functions

### Exception factory
The package provide a way to enhance errors of go.

An Exception consists on few properties:
* A message
* A code
* A fatal status

It's possible to instantiate Exception in two manners:

First one, instantiate through the `NewException` method, eg:
```golang
package main

import (
    "gitlab.com/hestia-go/core/exception"
)

func main() {
	exception := exception.NewException(exception.WithMessage("An error occurred"))
}
```

Second one, instantiate by wrapping a existing error, eg:
```golang
package main

import (
    "gitlab.com/hestia-go/core/exception"
)

func main() {
	err := funcReturningError()
	exception := exception.Wrap(err, exception.WithFatal(true))
}
```

### Registry
This package provide a registry mechanism with key/value TTL support.

This lets you store any volatile information during the application runtime.

Builtin registry driver:
* Memory

It's possible to create custom registry driver by implementing the `RegistryDriver` interface available in the subpackage `registry/interface`.

For a registry to be available, it needs to be registered through the `Register` function available in the subpackage `registry`.

### Utility
Under the `is` subpackage there is many utility function for runtime type checking like:
* Error
* Exception
* Struct
* Float
* Bool
* ...

Under the `runtime` subpackage the function `GetRuntimeDir` helps to get the binary directory.
Under the `slice` subpackage the function `Contains` helps to check if a slice contains a certain value.
