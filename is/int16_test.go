package is

import (
	"testing"
)

func TestInt16(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "int16 value",
			input:    int16(42),
			expected: true,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
		{
			name:     "int32 value",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "string value",
			input:    "42",
			expected: false,
		},
		{
			name:     "float32 value",
			input:    float32(42.0),
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Int16(tc.input); got != tc.expected {
				t.Errorf("Int16() = %v, want %v", got, tc.expected)
			}
		})
	}
}
