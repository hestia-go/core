package is

import (
	"testing"
)

func TestStruct(t *testing.T) {
	type testStruct struct {
		Name string
		Age  int
	}

	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "testStruct pointer",
			input:    &testStruct{},
			expected: true,
		},
		{
			name:     "testStruct value",
			input:    testStruct{},
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "float64 value",
			input:    3.14,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Struct(tc.input) != tc.expected {
				t.Errorf("Struct(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
