package is

import "reflect"

// Float64 Check if value is a float64
func Float64(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Float64
}
