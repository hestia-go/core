package is

import "reflect"

// Slice Check if value is a slice
func Slice(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Slice
}
