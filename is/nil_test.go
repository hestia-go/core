package is

import (
	"testing"

	"gitlab.com/hestia-go/core/iface"
)

func TestNil(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: true,
		},
		{
			name:     "bool",
			input:    true,
			expected: false,
		},
		{
			name:     "string",
			input:    "hello",
			expected: false,
		},
		{
			name:     "int",
			input:    42,
			expected: false,
		},
		{
			name:     "float32",
			input:    float32(3.14),
			expected: false,
		},
		{
			name:     "float64",
			input:    float64(3.14),
			expected: false,
		},
		{
			name:     "complex64",
			input:    complex64(1 + 2i),
			expected: false,
		},
		{
			name:     "complex128",
			input:    complex128(1 + 2i),
			expected: false,
		},
		{
			name:     "uint8",
			input:    uint8(42),
			expected: false,
		},
		{
			name:     "uint16",
			input:    uint16(42),
			expected: false,
		},
		{
			name:     "uint32",
			input:    uint32(42),
			expected: false,
		},
		{
			name:     "uint64",
			input:    uint64(42),
			expected: false,
		},
		{
			name:     "int8",
			input:    int8(42),
			expected: false,
		},
		{
			name:     "int16",
			input:    int16(42),
			expected: false,
		},
		{
			name:     "int32",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "int64",
			input:    int64(42),
			expected: false,
		},
		{
			name:     "uint",
			input:    uint(42),
			expected: false,
		},
		{
			name:     "uintptr",
			input:    uintptr(0),
			expected: false,
		},
		{
			name:     "struct",
			input:    struct{}{},
			expected: false,
		},
		{
			name:     "non-nil pointer",
			input:    new(int),
			expected: false,
		},
		{
			name:     "nil pointer",
			input:    (*int)(nil),
			expected: true,
		},
		{
			name:     "nil interface",
			input:    (iface.Exception)(nil),
			expected: true,
		},
		{
			name:     "nil function",
			input:    (func())(nil),
			expected: true,
		},
		{
			name: "nil map",
			input: (map[string]string)(nil),
			expected: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if output := Nil(tc.input); output != tc.expected {
				t.Errorf("Nil(%v) = %v; want %v", tc.input, output, tc.expected)
			}
		})
	}
}
