package is

import "reflect"

// Complex64 Check if value is a complex64
func Complex64(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Complex64
}
