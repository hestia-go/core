package is

import "reflect"

// Uint64 Check if value is uint64
func Uint64(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Uint64
}
