package is

import (
	"testing"
)

func TestUint(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "uint value",
			input:    uint(42),
			expected: true,
		},
		{
			name:     "uint8 value",
			input:    uint8(42),
			expected: false,
		},
		{
			name:     "uint16 value",
			input:    uint16(42),
			expected: false,
		},
		{
			name:     "uint32 value",
			input:    uint32(42),
			expected: false,
		},
		{
			name:     "uint64 value",
			input:    uint64(42),
			expected: false,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "float64 value",
			input:    3.14,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Uint(tc.input) != tc.expected {
				t.Errorf("Uint(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
