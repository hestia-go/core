package is

import "reflect"

// String Check if value is a string
func String(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.String
}
