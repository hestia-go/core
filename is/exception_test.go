package is

import (
	"testing"

	"gitlab.com/hestia-go/core/exception"
)

func TestException(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "exception value",
			input:    exception.NewException(),
			expected: true,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Exception(tc.input); got != tc.expected {
				t.Errorf("Exception(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
