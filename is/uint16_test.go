package is

import (
	"testing"
)

func TestUint16(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "uint16",
			input:    uint16(42),
			expected: true,
		},
		{
			name:     "uint32",
			input:    uint32(42),
			expected: false,
		},
		{
			name:     "uint64",
			input:    uint64(42),
			expected: false,
		},
		{
			name:     "int16",
			input:    int16(42),
			expected: false,
		},
		{
			name:     "int32",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "int64",
			input:    int64(42),
			expected: false,
		},
		{
			name:     "string",
			input:    "42",
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Uint16(tc.input) != tc.expected {
				t.Errorf("Uint16(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
