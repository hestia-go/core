package is

import "reflect"

// Bool Check if value is bool
func Bool(value any) bool {
	if Nil(value) {
		return false
	}
	return reflect.TypeOf(value).Kind() == reflect.Bool
}
