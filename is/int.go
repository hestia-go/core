package is

import "reflect"

// Int Check if value is an int
func Int(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Int
}
