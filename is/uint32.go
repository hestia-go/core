package is

import "reflect"

// Uint32 Check if value is a uint32
func Uint32(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Uint32
}
