package is

import (
	"testing"
)

func TestArray(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "array",
			input:    [3]int{1, 2, 3},
			expected: true,
		},
		{
			name:     "slice",
			input:    []int{1, 2, 3},
			expected: false,
		},
		{
			name:     "map",
			input:    map[string]int{"one": 1, "two": 2, "three": 3},
			expected: false,
		},
		{
			name:     "string",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Array(tc.input); got != tc.expected {
				t.Errorf("Array(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
