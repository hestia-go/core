package is

import (
	"testing"
)

func TestString(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "empty string",
			input:    "",
			expected: true,
		},
		{
			name:     "non-empty string",
			input:    "hello",
			expected: true,
		},
		{
			name:     "number",
			input:    42,
			expected: false,
		},
		{
			name:     "float",
			input:    3.14,
			expected: false,
		},
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if String(tc.input) != tc.expected {
				t.Errorf("String(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
