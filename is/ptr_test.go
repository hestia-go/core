package is

import (
	"testing"
)

func TestPtr(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "int pointer",
			input:    new(int),
			expected: true,
		},
		{
			name:     "float32 pointer",
			input:    new(float32),
			expected: true,
		},
		{
			name:     "string pointer",
			input:    new(string),
			expected: true,
		},
		{
			name:     "bool pointer",
			input:    new(bool),
			expected: true,
		},
		{
			name:     "struct pointer",
			input:    new(struct{}),
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "float64 value",
			input:    3.14,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Ptr(tc.input) != tc.expected {
				t.Errorf("Ptr(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
