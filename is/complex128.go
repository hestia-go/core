package is

import "reflect"

// Complex128 Check if value is a complex128
func Complex128(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Complex128
}
