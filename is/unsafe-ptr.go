package is

import "reflect"

// UnsafePtr Check if value is an unsafe pointer
func UnsafePtr(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.UnsafePointer
}
