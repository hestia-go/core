package is

import (
	"testing"
)

func TestUint32(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "uint32",
			input:    uint32(42),
			expected: true,
		},
		{
			name:     "uint",
			input:    uint(42),
			expected: false,
		},
		{
			name:     "uint16",
			input:    uint16(42),
			expected: false,
		},
		{
			name:     "int32",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "float32",
			input:    float32(3.14),
			expected: false,
		},
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Uint32(tc.input) != tc.expected {
				t.Errorf("Uint32(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
