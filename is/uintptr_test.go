package is

import (
	"testing"
)

func TestUintPtr(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "uintptr value",
			input:    uintptr(42),
			expected: true,
		},
		{
			name:     "int pointer",
			input:    new(int),
			expected: false,
		},
		{
			name:     "uintptr pointer",
			input:    new(uintptr),
			expected: false,
		},
		{
			name:     "string pointer",
			input:    new(string),
			expected: false,
		},
		{
			name:     "struct pointer",
			input:    new(struct{}),
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if UintPtr(tc.input) != tc.expected {
				t.Errorf("UintPtr(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
