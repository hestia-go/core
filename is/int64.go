package is

import "reflect"

// Int64 Check if value is an int64
func Int64(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Int64
}
