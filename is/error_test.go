package is

import (
	"errors"
	"testing"
)

func TestError(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "error value",
			input:    errors.New("some error"),
			expected: true,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Error(tc.input); got != tc.expected {
				t.Errorf("Error(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
