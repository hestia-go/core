package is

import (
	"testing"
)

func TestBool(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "boolean value",
			input:    true,
			expected: true,
		},
		{
			name:     "string value",
			input:    "true",
			expected: false,
		},
		{
			name:     "integer value",
			input:    1,
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Bool(tc.input); got != tc.expected {
				t.Errorf("Bool(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
