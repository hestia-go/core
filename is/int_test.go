package is

import (
	"testing"
)

func TestInt(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "int",
			input:    42,
			expected: true,
		},
		{
			name:     "int8",
			input:    int8(42),
			expected: false,
		},
		{
			name:     "int16",
			input:    int16(42),
			expected: false,
		},
		{
			name:     "int32",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "int64",
			input:    int64(42),
			expected: false,
		},
		{
			name:     "uint",
			input:    uint(42),
			expected: false,
		},
		{
			name:     "uint8",
			input:    uint8(42),
			expected: false,
		},
		{
			name:     "uint16",
			input:    uint16(42),
			expected: false,
		},
		{
			name:     "uint32",
			input:    uint32(42),
			expected: false,
		},
		{
			name:     "uint64",
			input:    uint64(42),
			expected: false,
		},
		{
			name:     "float32",
			input:    float32(42),
			expected: false,
		},
		{
			name:     "float64",
			input:    float64(42),
			expected: false,
		},
		{
			name:     "string",
			input:    "42",
			expected: false,
		},
		{
			name:     "bool",
			input:    true,
			expected: false,
		},
		{
			name:     "struct",
			input:    struct{}{},
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if output := Int(tc.input); output != tc.expected {
				t.Errorf("Int(%v) = %v; want %v", tc.input, output, tc.expected)
			}
		})
	}
}

