package is

import "reflect"

// Int32 Check if value is int32
func Int32(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Int32
}
