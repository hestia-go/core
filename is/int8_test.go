package is

import (
	"testing"
)

func TestInt8(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "Int8",
			input:    int8(42),
			expected: true,
		},
		{
			name:     "Int",
			input:    42,
			expected: false,
		},
		{
			name:     "Int16",
			input:    int16(42),
			expected: false,
		},
		{
			name:     "Int32",
			input:    int32(42),
			expected: false,
		},
		{
			name:     "Int64",
			input:    int64(42),
			expected: false,
		},
		{
			name:     "Uint8",
			input:    uint8(42),
			expected: false,
		},
		{
			name:     "Nil",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Int8(tc.input); got != tc.expected {
				t.Errorf("Int8(%v) = %v, expected %v", tc.input, got, tc.expected)
			}
		})
	}
}
