package is

import "reflect"

// Uint8 Check if value is a uint8
func Uint8(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Uint8
}
