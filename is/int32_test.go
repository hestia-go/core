package is

import (
	"testing"
)

func TestInt32(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "int32 value",
			input:    int32(42),
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "42",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if output := Int32(tc.input); output != tc.expected {
				t.Errorf("Int32(%v) = %v; want %v", tc.input, output, tc.expected)
			}
		})
	}
}

