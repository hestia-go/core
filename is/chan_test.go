package is

import (
	"testing"
)

func TestChan(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "unbuffered channel",
			input:    make(chan int),
			expected: true,
		},
		{
			name:     "buffered channel",
			input:    make(chan string, 5),
			expected: true,
		},
		{
			name:     "map value",
			input:    map[string]int{"one": 1, "two": 2, "three": 3},
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Chan(tc.input); got != tc.expected {
				t.Errorf("Chan(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
