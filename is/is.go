// Package is provides functions for type checking at Runtime
package is

import "reflect"

// Is Check if the value is the type of given generic
func Is(value any, expected any) bool {
	return reflect.TypeOf(value) == reflect.TypeOf(expected)
}
