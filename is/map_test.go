package is

import (
	"testing"
)

func TestMap(t *testing.T) {
	tests := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "map[string]int",
			input:    map[string]int{"a": 1, "b": 2},
			expected: true,
		},
		{
			name:     "map[int]string",
			input:    map[int]string{1: "a", 2: "b"},
			expected: true,
		},
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "int",
			input:    42,
			expected: false,
		},
		{
			name:     "string",
			input:    "foo",
			expected: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			if output := Map(tc.input); output != tc.expected {
				t.Errorf("Map(%v) = %v; want %v", tc.input, output, tc.expected)
			}
		})
	}
}

