package is

import "reflect"

// Chan Check if value is a channel
func Chan(value any) bool {
	if Nil(value) {
		return false
	}
	return reflect.TypeOf(value).Kind() == reflect.Chan
}
