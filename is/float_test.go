package is

import (
	"math"
	"testing"
)

func TestFloat(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "float32 value",
			input:    float32(math.Pi),
			expected: true,
		},
		{
			name:     "float64 value",
			input:    math.Pi,
			expected: true,
		},
		{
			name:     "string value",
			input:    "3.14",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Float(tc.input); got != tc.expected {
				t.Errorf("Float(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}

