package is

import (
	"testing"
)

func TestUint64(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "uint64",
			input:    uint64(42),
			expected: true,
		},
		{
			name:     "int64",
			input:    int64(42),
			expected: false,
		},
		{
			name:     "string",
			input:    "42",
			expected: false,
		},
		{
			name:     "float64",
			input:    42.0,
			expected: false,
		},
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "struct",
			input:    struct{}{},
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Uint64(tc.input); got != tc.expected {
				t.Errorf("Uint64(%v) = %v; want %v", tc.input, got, tc.expected)
			}
		})
	}
}

