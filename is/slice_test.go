package is

import (
	"testing"
)

func TestSlice(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "int slice",
			input:    []int{1, 2, 3},
			expected: true,
		},
		{
			name:     "string slice",
			input:    []string{"hello", "world"},
			expected: true,
		},
		{
			name:     "bool slice",
			input:    []bool{true, false},
			expected: true,
		},
		{
			name:     "struct slice",
			input:    []struct{}{{}, {}},
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "float64 value",
			input:    3.14,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Slice(tc.input) != tc.expected {
				t.Errorf("Slice(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
