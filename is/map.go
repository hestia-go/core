package is

import "reflect"

// Map Check if value is a map
func Map(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Map
}
