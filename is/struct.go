package is

import "reflect"

// Struct Check if value is a structure
func Struct(value any) bool {
	typ := reflect.TypeOf(value)

	if typ == nil {
		return false
	}

	if Ptr(value) {
		typ = typ.Elem()
	}

	return typ.Kind() == reflect.Struct
}
