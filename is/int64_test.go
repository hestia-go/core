package is

import (
	"testing"
)

func TestInt64(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "int64 value",
			input:    int64(42),
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "42",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Int64(tc.input) != tc.expected {
				t.Errorf("Expected Int64(%v) to return %v, but got %v", tc.input, tc.expected, !tc.expected)
			}
		})
	}
}

