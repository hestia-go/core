package is

import (
	"testing"
)

func TestFunc(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "Function value",
			input:    func() {},
			expected: true,
		},
		{
			name:     "Method value",
			input:    (*testing.T).FailNow,
			expected: true,
		},
		{
			name:     "Non-function value",
			input:    "not a function",
			expected: false,
		},
		{
			name:     "Nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Func(tc.input) != tc.expected {
				t.Errorf("Expected Func to return %v for input %v, but got %v", tc.expected, tc.input, !tc.expected)
			}
		})
	}
}
