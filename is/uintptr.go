package is

import "reflect"

// UintPtr Check if value is uint pointer
func UintPtr(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Uintptr
}
