package is

import "reflect"

// Func Check if value is a function
func Func(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Func
}
