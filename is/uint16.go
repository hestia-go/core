package is

import "reflect"

// Uint16 Check if value is a uint16
func Uint16(value any) bool {
	return reflect.TypeOf(value).Kind() == reflect.Uint16
}
