package is

import (
	"gitlab.com/hestia-go/core/iface"
)

// Exception Check if value is an exception
func Exception(value any) bool {
	if val, ok := value.(iface.Exception); ok {
		return val != nil
	}

	return false
}
