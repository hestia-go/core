package is

import "reflect"

// Ptr Check if value is a pointer
func Ptr(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Ptr
}
