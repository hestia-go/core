package is

import "reflect"

// Uint Check if value is a uint
func Uint(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Uint
}
