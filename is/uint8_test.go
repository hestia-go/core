package is

import (
	"testing"
)

func TestUint8(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "uint8 value",
			input:    uint8(42),
			expected: true,
		},
		{
			name:     "uint value",
			input:    uint(42),
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if Uint8(tc.input) != tc.expected {
				t.Errorf("Uint8(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
