package is

import "reflect"

// Int16 Check if value is an int16
func Int16(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Int16
}
