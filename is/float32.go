package is

import "reflect"

// Float32 Check if value is a float32
func Float32(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Float32
}
