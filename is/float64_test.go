package is

import (
	"math"
	"testing"
)

func TestFloat64(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "Float32 value",
			input:    float32(math.Pi),
			expected: false,
		},
		{
			name:     "Float64 value",
			input:    math.Pi,
			expected: true,
		},
		{
			name:     "String value",
			input:    "3.14",
			expected: false,
		},
		{
			name:     "Nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			if Float64(test.input) != test.expected {
				t.Errorf("Expected Float64 to return %v for input %v, but got %v", test.expected, test.input, !test.expected)
			}
		})
	}
}

