package is

import (
	"reflect"
)

// Error Check if value is an error
func Error(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Implements(reflect.TypeOf((*error)(nil)).Elem())
}
