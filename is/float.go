package is

// Float Check if value is a float
func Float(value any) bool {
	if Nil(value) {
		return false
	}

	return Float32(value) || Float64(value)
}
