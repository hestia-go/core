package is

import "reflect"

// Nil Check if value is nil
func Nil(value any) bool {
	if value == nil ||
		((reflect.ValueOf(value).Kind() == reflect.Ptr ||
			reflect.ValueOf(value).Kind() == reflect.Map ||
			reflect.ValueOf(value).Kind() == reflect.Func) &&
			reflect.ValueOf(value).IsNil()) {
		return true
	}

	switch reflect.ValueOf(value).Type().Kind() {
	case reflect.Bool,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Uintptr,
		reflect.Float32,
		reflect.Float64,
		reflect.Complex64,
		reflect.Complex128,
		reflect.Array,
		reflect.Chan,
		reflect.Func,
		reflect.Interface,
		reflect.Map,
		reflect.Pointer,
		reflect.Slice,
		reflect.String,
		reflect.Struct,
		reflect.UnsafePointer:
		return false
	}

	// Added skipcq, this code path is unreachable
	return true // skipcq: TCV-001
}
