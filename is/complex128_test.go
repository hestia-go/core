package is

import (
	"testing"
)

func TestComplex128(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "complex128 value",
			input:    complex(2.5, 3.5),
			expected: true,
		},
		{
			name:     "complex64 value",
			input:    complex(float32(2.5), float32(3.5)),
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "nil value",
			input:    nil,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Complex128(tc.input); got != tc.expected {
				t.Errorf("Complex128(%v) = %v, want %v", tc.input, got, tc.expected)
			}
		})
	}
}
