package is

import "reflect"

// Int8 Check if value is an int8
func Int8(value any) bool {
	if Nil(value) {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Int8
}
