package is

import (
	"reflect"
)

// Array Check if value is array
func Array(value any) bool {
	if value == nil {
		return false
	}

	return reflect.TypeOf(value).Kind() == reflect.Array
}
