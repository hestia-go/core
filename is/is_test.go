package is

import (
	"testing"
)

func TestIs(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected interface{}
		want     bool
	}{
		{
			name:     "int is int",
			input:    42,
			expected: int(0),
			want:     true,
		},
		{
			name:     "string is string",
			input:    "hello",
			expected: "",
			want:     true,
		},
		{
			name:     "float is float",
			input:    3.14,
			expected: float64(0),
			want:     true,
		},
		{
			name:     "struct is struct",
			input:    struct{}{},
			expected: struct{}{},
			want:     true,
		},
		{
			name:     "int is not string",
			input:    42,
			expected: "",
			want:     false,
		},
		{
			name:     "float is not int",
			input:    3.14,
			expected: int(0),
			want:     false,
		},
		{
			name:     "struct is not string",
			input:    struct{}{},
			expected: "",
			want:     false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := Is(tc.input, tc.expected); got != tc.want {
				t.Errorf("Is(%v, %v) = %v; want %v", tc.input, tc.expected, got, tc.want)
			}
		})
	}
}
