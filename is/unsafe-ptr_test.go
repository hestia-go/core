package is

import (
	"testing"
	"unsafe"
)

func TestUnsafePtr(t *testing.T) {
	testCases := []struct {
		name     string
		input    interface{}
		expected bool
	}{
		{
			name:     "nil",
			input:    nil,
			expected: false,
		},
		{
			name:     "int pointer",
			input:    new(int),
			expected: false,
		},
		{
			name:     "float32 pointer",
			input:    new(float32),
			expected: false,
		},
		{
			name:     "string pointer",
			input:    new(string),
			expected: false,
		},
		{
			name:     "bool pointer",
			input:    new(bool),
			expected: false,
		},
		{
			name:     "struct pointer",
			input:    new(struct{}),
			expected: false,
		},
		{
			name:     "unsafe pointer",
			// Added skipcq, this unsafe usage of unsafe pointer is expected for test case
			input:    unsafe.Pointer(uintptr(0)), // skipcq: VET-V0020
			expected: true,
		},
		{
			name:     "int value",
			input:    42,
			expected: false,
		},
		{
			name:     "string value",
			input:    "hello",
			expected: false,
		},
		{
			name:     "float64 value",
			input:    3.14,
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if UnsafePtr(tc.input) != tc.expected {
				t.Errorf("UnsafePtr(%v) = %v; want %v", tc.input, !tc.expected, tc.expected)
			}
		})
	}
}
