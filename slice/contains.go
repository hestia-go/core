// Package slice provides helper functions to work with slice
package slice

import "gitlab.com/hestia-go/core/is"

// Contains Return if the given slice contains the expected value
func Contains[T comparable](slice []T, expected T) bool {
	if !is.Slice(slice) || len(slice) == 0 {
		return false
	}

	for _, i := range slice {
		if i == expected {
			return true
		}
	}

	return false
}
