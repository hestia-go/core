package slice

import (
	"testing"
)

func TestContains(t *testing.T) {
	// Test with an integer slice
	intSlice := []int{1, 2, 3, 4, 5}
	if !Contains(intSlice, 2) {
		t.Error("Expected to find 2 in the slice")
	}
	if Contains(intSlice, 6) {
		t.Error("Did not expect to find 6 in the slice")
	}
	if !Contains(intSlice, 1) {
		t.Error("Expected to find 1 in the slice")
	}

	// Test with a string slice
	stringSlice := []string{"apple", "banana", "orange"}
	if !Contains(stringSlice, "banana") {
		t.Error("Expected to find 'banana' in the slice")
	}
	if Contains(stringSlice, "grape") {
		t.Error("Did not expect to find 'grape' in the slice")
	}
	if !Contains(stringSlice, "apple") {
		t.Error("Expected to find 'apple' in the slice")
	}

	if Contains[string]([]string{}, "test") {
		t.Error("Expected to return false if the slice is empty")
	}

	if Contains[string](nil, "test") {
		t.Error("Expected to return false if the slice value is not a slice")
	}
}
