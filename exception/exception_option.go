package exception

// ExceptionOption type
type ExceptionOption func(*Exception)

// WithCode set the code in the exception
func WithCode(code string) ExceptionOption {
	return func(e *Exception) {
		e.code = code
	}
}

// WithFatal set the fatal state of the exception to true
func WithFatal() ExceptionOption {
	return func(e *Exception) {
		e.fatal = true
	}
}

// WithInnerError set the inner error in the exception
func WithInnerError(err error) ExceptionOption {
	return func(e *Exception) {
		e.err = err
	}
}

// WithMessage set the exception message
func WithMessage(message string) ExceptionOption {
	return func(e *Exception) {
		e.message = message
	}
}
