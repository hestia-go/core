package exception_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/exception"
)

func ExampleException_Error() {
	exc := exception.NewException(exception.WithMessage("example message"))

	fmt.Println(exc.Error())

	// Output:
	// example message
}
