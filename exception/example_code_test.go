package exception_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/exception"
)

func ExampleException_Code() {
	exc := exception.NewException(exception.WithCode("my_code"))

	fmt.Println(exc.Code())

	// Output:
	// my_code
}
