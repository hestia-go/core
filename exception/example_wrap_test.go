package exception_test

import (
	"errors"
	"fmt"

	"gitlab.com/hestia-go/core/exception"
)

func ExampleWrap() {
	err := errors.New("test error")

	exc := exception.Wrap(err, exception.WithCode("my_exc"))

	fmt.Println(exc)

	// Output:
	// test error
}
