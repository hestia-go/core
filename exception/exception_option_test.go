package exception

import (
	"errors"
	"testing"
)

func TestWithCode(t *testing.T) {
	e := &Exception{}
	code := "test-code"
	WithCode(code)(e)
	if e.code != code {
		t.Errorf("WithCode did not set the correct code. Expected %s, got %s", code, e.code)
	}
}

func TestWithFatal(t *testing.T) {
	e := &Exception{}
	WithFatal()(e)
	if !e.fatal {
		t.Error("WithFatal did not set the fatal flag to true")
	}
}

func TestWithInnerError(t *testing.T) {
	e := &Exception{}
	err := errors.New("test error")
	WithInnerError(err)(e)
	if e.err != err {
		t.Error("WithInnerError did not set the correct inner error")
	}
}

func TestWithMessage(t *testing.T) {
	e := &Exception{}
	message := "test message"
	WithMessage(message)(e)
	if e.message != message {
		t.Errorf("WithMessage did not set the correct message. Expected %s, got %s", message, e.message)
	}
}
