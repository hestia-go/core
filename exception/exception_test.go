package exception

import (
	"errors"
	"testing"
)

func TestException(t *testing.T) {
	err := errors.New("inner error")
	e := Exception{
		message: "Something went wrong",
		code:    "ERR001",
		fatal:   true,
		err:     err,
	}

	if e.message != "Something went wrong" {
		t.Errorf("Expected message 'Something went wrong', but got %s", e.message)
	}
	if e.code != "ERR001" {
		t.Errorf("Expected code ERR001, but got %s", e.code)
	}
	if !e.fatal {
		t.Error("Expected fatal to be true, but got false")
	}
	if e.err != err {
		t.Error("Expected inner error to be 'inner error', but got something else")
	}

	var errorAny any = &e

	if _, ok := errorAny.(error); !ok {
		t.Error("Expected exception to be convertible to error, but got false")
	}
}

func TestCode(t *testing.T) {
	e := NewException(WithCode("ERR001"))
	if e.Code() != "ERR001" {
		t.Errorf("Expected code ERR001, but got %s", e.Code())
	}
}

func TestError(t *testing.T) {
	e := NewException(WithMessage("Something went wrong"))
	if e.Error() != "Something went wrong" {
		t.Errorf("Expected error message 'Something went wrong', but got %s", e.Error())
	}
}

func TestFatal(t *testing.T) {
	e := NewException(WithFatal())
	if !e.Fatal() {
		t.Error("Expected fatal to be true, but got false")
	}
}

func TestMessage(t *testing.T) {
	e := NewException(WithMessage("Something went wrong"))
	if e.Message() != "Something went wrong" {
		t.Errorf("Expected message 'Something went wrong', but got %s", e.Message())
	}
}

func TestUnwrap(t *testing.T) {
	err := errors.New("inner error")
	e := NewException(WithInnerError(err))
	if e.Unwrap() != err {
		t.Error("Expected unwrapped error to be 'inner error', but got something else")
	}
}

func TestNewException(t *testing.T) {
	e := NewException(
		WithCode("ERR001"),
		WithMessage("Something went wrong"),
		WithFatal(),
		WithInnerError(errors.New("inner error")),
	)
	if e.Code() != "ERR001" {
		t.Errorf("Expected code ERR001, but got %s", e.Code())
	}
	if e.Message() != "Something went wrong" {
		t.Errorf("Expected message 'Something went wrong', but got %s", e.Message())
	}
	if !e.Fatal() {
		t.Error("Expected fatal to be true, but got false")
	}
	if e.Unwrap().Error() != "inner error" {
		t.Error("Expected unwrapped error to be 'inner error', but got something else")
	}
}

func TestWrap(t *testing.T) {
	err := errors.New("inner error")
	e := Wrap(err, WithCode("ERR001"))
	if e.Code() != "ERR001" {
		t.Errorf("Expected code ERR001, but got %s", e.Code())
	}
	if e.Unwrap() != err {
		t.Error("Expected unwrapped error to be 'inner error', but got something else")
	}
}
