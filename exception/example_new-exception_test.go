package exception_test

import (
	"fmt"

	"gitlab.com/hestia-go/core/exception"
)

func ExampleNewException() {
	exc := exception.NewException(exception.WithMessage("example exception"), exception.WithCode("fatal_example"), exception.WithFatal())

	fmt.Println(exc)

	// Output:
	// example exception
}
