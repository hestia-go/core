package exception_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/exception"
)

func ExampleException_Message() {
	exc := exception.NewException(exception.WithMessage("example message"))

	fmt.Println(exc.Message())

	// Output:
	// example message
}
