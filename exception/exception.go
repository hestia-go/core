// Package exception provides exception factory
// The package provide a way to enhance errors of go.
//
// An Exception consists on few properties: A message, a code, and a fatal status
//
// It's possible to instantiate Exception in two manners:
//
// First one, instantiate through the `NewException` method, eg:
//
//	exception := exception.NewException(WithCode("ERR00001"), WithMessage("Exception message"))
//
// Second one, instantiate by wrapping a existing error, eg:
//
//	err := funcReturningError()
//	exception := exception.Wrap(err, exception.WithFatal())
package exception

// Exception structure
type Exception struct {
	message string
	code    string
	fatal   bool
	err     error
}

// Code return the exception code
func (e *Exception) Code() string {
	return e.code
}

// Error return the exception message (implemented for error interface)
func (e *Exception) Error() string {
	return e.Message()
}

// Fatal return the fatal state
func (e *Exception) Fatal() bool {
	return e.fatal
}

// Message return the exception message
func (e *Exception) Message() string {
	return e.message
}

// Unwrap return the inner error
func (e *Exception) Unwrap() error {
	return e.err
}

// NewException create a new exception with the provided option
func NewException(opts ...ExceptionOption) *Exception {
	var exception = new(Exception)

	for _, opt := range opts {
		opt(exception)
	}

	return exception
}

// Wrap an error by creating a new exception
func Wrap(err error, opts ...ExceptionOption) *Exception {
	opts = append(opts, WithMessage(err.Error()), WithInnerError(err))

	return NewException(opts...)
}
