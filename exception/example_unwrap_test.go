package exception_test

import (
	"errors"
	"fmt"

	"gitlab.com/hestia-go/core/exception"
)

func ExampleException_Unwrap() {
	exc := exception.Wrap(errors.New("example error"))

	innerError := exc.Unwrap()

	fmt.Println(innerError.Error())

	// Output:
	// example error
}
