package exception_test

import (
	"fmt"
	"gitlab.com/hestia-go/core/exception"
)

func ExampleException_Fatal() {
	exc := exception.NewException(exception.WithFatal())

	fmt.Println(exc.Fatal())

	// Output:
	// true
}
