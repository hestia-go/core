package runtime

import (
	"testing"
)

func TestGetRuntimeDir(t *testing.T) {
	// Test when there is no error
	actualDir, err := GetRuntimeDir()

	if err != nil {
		t.Errorf("Expected nil error but got %v", err)
	}

	if actualDir == "" {
		t.Errorf("Expected executable directory but got empty path")
	}
}
