// Package runtime provides helper functions for Runtime information
package runtime

import (
	"os"
	"path/filepath"

	"gitlab.com/hestia-go/core/is"
)

// GetRuntimeDir Return the executable root path
func GetRuntimeDir() (string, error) {
	ex, err := os.Executable()

	// Added skipcq, this code path is unreachable by test
	if !is.Nil(err) { // skipcq: TCV-001
		return "", err // skipcq: TCV-001
	}

	return filepath.Dir(ex), nil
}
