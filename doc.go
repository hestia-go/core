// Package core contains tools that becomes available in all Hestia based projects.
//
// It includes sub package: Exception factory, Registry, Utility functions
package core

// blank imports for docs

// constants
import _ "gitlab.com/hestia-go/core/constant"

// interface
import _ "gitlab.com/hestia-go/core/iface"

// exception
import _ "gitlab.com/hestia-go/core/exception"

// registry
import _ "gitlab.com/hestia-go/core/registry"

// registry interface
import _ "gitlab.com/hestia-go/core/registry/interface"

// registry memory implementation
import _ "gitlab.com/hestia-go/core/registry/driver/memory"

// type checking utils
import _ "gitlab.com/hestia-go/core/is"

// runtime utils
import _ "gitlab.com/hestia-go/core/runtime"

// slice utils
import _ "gitlab.com/hestia-go/core/slice"
