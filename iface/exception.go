package iface

// Exception interface to implement for custom exception
type Exception interface {
	// Code returns the exception code
	Code() string
	// Fatal returns the exception fatal state
	Fatal() bool
	// Message returns the exception message
	Message() string
	// Unwrap returns the original error
	Unwrap() error
	// Error returns the exception message
	Error() string
}
