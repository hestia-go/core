package constant_test

import (
	"testing"

	"gitlab.com/hestia-go/core/constant"
)

func TestRegistryMemoryDriverToken(t *testing.T) {
	// Test if RegistryMemoryDriverToken is equal to "memory"
	if constant.RegistryMemoryDriverToken != "memory" {
		t.Errorf("RegistryMemoryDriverToken should be equal to 'memory'")
	}
}
