package constant

const (
	// RegistryMemoryDriverToken Token of registry memory driver
	RegistryMemoryDriverToken = "memory"
)
